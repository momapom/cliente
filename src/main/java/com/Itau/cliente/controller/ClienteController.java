package com.Itau.cliente.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.Itau.cliente.model.Cliente;
import com.Itau.cliente.repository.ClienteRepository;

@Controller
public class ClienteController {
	
	@Autowired
	ClienteRepository clienteRepository;
	
	@CrossOrigin
	@RequestMapping(path="/cliente", method=RequestMethod.POST)
	public ResponseEntity<?> cadastrarCliente(HttpServletRequest request, @RequestBody Cliente cliente) {

		clienteRepository.save(cliente);
		return ResponseEntity.ok(cliente);
		}
	
	@CrossOrigin
	@RequestMapping(path="/cliente/{id}", method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> getClienteById(@PathVariable int id) {
		Optional<Cliente> clienteBanco = clienteRepository.findById(id);
		if(! clienteBanco.isPresent()) {
//			return ResponseEntity.badRequest().build();
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(clienteRepository.findById(id));
		}

	@CrossOrigin
	@RequestMapping(path="/removerCliente/{id}", method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> getRemoveCliente(@PathVariable int id) {
		Optional<Cliente> clienteBanco = clienteRepository.findById(id);
		if(! clienteBanco.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		clienteRepository.deleteById(id);
		return ResponseEntity.ok(clienteRepository.findById(id));
		}

	// testando para gerar documentação
	@CrossOrigin
	@RequestMapping(path="/cliente", method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> getClienteById() {
		return ResponseEntity.ok(clienteRepository.findAll());
	}

}
