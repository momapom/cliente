**Cliente**
----
  <_API para cadastro de clientes que estão efetuando compra no icommerce_>

* **URL**

  <_/cliente_>

* **Method:**
  
  <_/cliente_>

   `POST` 

  <_/cliente/{id}_>
  
   `GET`

*  **URL Params**


   **Required:**
 
   `id=[integer]`


* **Data Params**

{
        "nome": "Marie Curie",
        "telefone": "21 86567-8730",
        "email": "marie.curie@uol.com"
} 

* **Success Response:**
  
  <_What should the status code be on success and is there any returned data? This is useful when people need to to know what their callbacks should expect!_>

  * **Code:** 200 <br />
    **Content:** `{ id : 1 }`
 
* **Error Response:**

  <_Most endpoints will have many ways they can fail. From unauthorized access, to wrongful parameters etc. All of those should be liste d here. It might seem repetitive, but it helps prevent assumptions from being made where they should be._>

  * **Code:** 400 BADREQUEST <br />
   
* **Sample Call:**

localhost:8080/cliente
 

* **Notes:**

  <_This is where all uncertainties, commentary, discussion etc. can go. I recommend timestamping and identifying oneself when leaving comments here._>
ae
